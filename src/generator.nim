import std/strutils
import std/strbasics
import std/tables

import lexer_types
import parser

type
  Generator* = object 
    tokens*: string
    token_datastore*: TokenDatastore


proc find_and_append(gen: var Generator, tokens: string, 
  token_datastore: TokenDatastore, first, last: int, parse_proc: proc): int =
  var bounds = parse_proc(tokens[first..last])
  if bounds.first != -1:
    bounds.first += first
    bounds.last += first
    for idx in bounds.first..bounds.last:
      result += 1
      gen.tokens.add(tokens[idx])
      if token_datastore.hasKey(idx):
        gen.token_datastore[gen.tokens.high] = token_datastore[idx]

proc append_new_tokens(gen: var Generator, token_kinds: seq[TokenKind]): int =
  for token_kind in token_kinds:
    gen.tokens &= translate(token_kind)
    result += 1

proc strip_end(tokens: var string): int =
  result = tokens.high
  tokens.strip(leading = false, trailing = true, chars = Whitespace)
  result -= tokens.high 

proc fix*(gen: var Generator, tokens: string, token_datastore: TokenDatastore,
  start_token_kind: TokenKind, first = 0, last = tokens.high): int =
  template fap(parse_proc: proc) =
    result += gen.find_and_append(tokens, token_datastore, first, last, 
      parse_proc)
  case start_token_kind
  of TokenKind.move:
    fap(find_lhs_in_move)
    result += gen.append_new_tokens(@[wspace, equals, wspace])
    fap(find_rhs_in_move)
    result -= gen.tokens.strip_end()
    result += gen.append_new_tokens(@[dot, wspace])
    fap(find_trailing_comment)
    result += gen.append_new_tokens(@[linebreak])
  of TokenKind.add:
    fap(find_lhs_in_add)
    result += gen.append_new_tokens(@[wspace, plus, wspace])
    fap(find_rhs_in_add)
    result -= gen.tokens.strip_end()
    result += gen.append_new_tokens(@[dot, wspace])
    fap(find_trailing_comment)
    result += gen.append_new_tokens(@[linebreak])
  else: discard