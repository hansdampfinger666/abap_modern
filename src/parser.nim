import std/re
import std/tables

import lexer_types


type
  SearchPatterns {.pure.} = enum
    rhs_in_move
    lhs_in_move
    rhs_in_add 
    lhs_in_add 
    select_statement
    select_list

# rhs in move (including possible line breaks): (?<=m )(.+|.*\n.*)(?=t)
# lhs in move: (?<=t\s).+(?=\.)
# rhs in add (including possible line breaks): (?<=a )(.+|.*\n.*)(?=t)
# select statement: s[SD\s]+ 
# select list: (?<=s)[Sd\s]+\K.*(?=F)

const
  regexes* = {
    SearchPatterns.rhs_in_move: r"(?<=" & translate(TokenKind.move) & 
      " )(.+|.*\n.*)(?=" & translate(TokenKind.to) & ")",
    SearchPatterns.lhs_in_move: r"(?<=" & translate(TokenKind.to) & " ).+(?=.)",
    SearchPatterns.rhs_in_add: r"(?<=" & translate(TokenKind.add) & 
      " )(.+|.*\n.*)(?=" & translate(TokenKind.to) & ")",
    SearchPatterns.lhs_in_add: r"(?<=" & translate(TokenKind.to) & " ).*(?=.)",

    SearchPatterns.select_statement: r"" & translate(TokenKind.select) & "[" & 
      translate(TokenKind.single) & translate(TokenKind.tdistinct) & "\\s]+",
    SearchPatterns.select_list: r"(?<=" & translate(TokenKind.select) & ")[" & 
      translate(TokenKind.single) & translate(TokenKind.tdistinct) &
      "\\s]+\\K.*(?=" & translate(TokenKind.tfrom) & ")",
  }.toTable()


proc tokens_conclude_statement*(tokens: string, rev_to = 0): bool =
  if tokens.len == 0: return
  var idx = tokens.high
  while idx >= rev_to:
    case tokens[idx].translate()
    of [TokenKind.wspace, TokenKind.trailing_comment]:
      dec idx
      continue
    of TokenKind.dot: return true
    else: return false

proc find_rhs_in_move*(tokens: string): tuple[first, last: int] =
  result = findBounds(tokens, re(regexes[rhs_in_move]))

proc find_lhs_in_move*(tokens: string): tuple[first, last: int] =
  result = findBounds(tokens, re(regexes[lhs_in_move]))

proc find_rhs_in_add*(tokens: string): tuple[first, last: int] =
  echo "rhs in add: ", regexes[rhs_in_add]
  result = findBounds(tokens, re(regexes[rhs_in_add]))

proc find_lhs_in_add*(tokens: string): tuple[first, last: int] =
  result = findBounds(tokens, re(regexes[lhs_in_add]))

proc find_select_statement*(tokens: string): tuple[first, last: int] =
  result = findBounds(tokens, re(regexes[select_statement]))

proc find_select_list*(tokens: string): tuple[first, last: int] =
  result = findBounds(tokens, re(regexes[select_list]))

proc find_trailing_comment*(tokens: string): tuple[first, last: int] =
  if tokens[tokens.high].translate() == trailing_comment:
    result = (tokens.high, tokens.high)
  else: result = (-1, 0)