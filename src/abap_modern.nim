import os
import std/strutils
import std/streams

import lexer
import lexer_types
import parser
import generator


proc main =


  # file handling
  let 
    filename = getCurrentDir() & "/LZ_QM_Y2F01.abap"
    file = open(filename)
  defer: file.close()

  var new_filename = filename
  new_filename.insert("_new", new_filename.rfind("."))

  if fileExists(new_filename): removeFile(new_filename)

  # tokenization loop prep
  var 
    new_file = newFileStream(new_filename, fmWrite)
    lex = initLexer()
    gen = Generator()
    loops = 0
    line: string
    statement: string
    fixed_line: string

  const max_loops = 6
  defer: new_file.close()

  # main file loop
  while not endOfFile(file):
    line = readLine(file)
    inc lex.line_number

    # check if we have to tokenize
    if (lex.current_start_tok_kind == none) and
      (not lex.line_starts_with_start_token(line)):
      writeLine(new_file, line)
      continue

    # reset stuff
    reset_lexer(lex)
    lex.input = line
    statement &= line

    # check max loop early return
    if (max_loops != -1) and (loops == max_loops): 
      echo "_________________________________________________________________________________"
      echo "_________________________________________________________________________________"
      echo "max loops reached"
      break

    # read chars and tokenize line
    while lex.read_char(): 
      lex.next_token()

    # check and set continuation conditions
    if lex.tokens.tokens_conclude_statement(): 
      let added_lines = gen.fix(lex.tokens, lex.token_datastore, 
        lex.current_start_tok_kind, lex.tok_idx_first, lex.tok_idx_last)
      let gen_first = gen.tokens.high - added_lines + 1
      echo "added lines: ", added_lines
      fixed_line = create_string_from_tokenstream(gen.tokens, 
        gen.token_datastore, gen_first, gen.tokens.high)
      writeLine(new_file, fixed_line)
      lex.current_start_tok_kind = none

      # results of statement tokenization
      echo "|", lex.tokens[lex.tok_idx_first..lex.tok_idx_last], "|"
      print_token_data(lex.token_datastore, lex.tok_idx_first, lex.tok_idx_last)
      echo "original line: "
      echo "|", statement, "|"
      echo "FIXED:"
      echo "|", gen.tokens[gen_first..gen.tokens.high], "|"
      print_token_data(gen.token_datastore, gen_first, gen.tokens.high)
      echo "fixed line: "
      echo "|", fixed_line, "|"
      fixed_line = ""
      statement = ""
      lex.advance_tok_indexes()

    inc loops
    lex.add_token(Token(kind: linebreak))
    echo "______________________________________________"
  echo "______________________________________________"

  # print final results
  echo "_________________________________________________________________________________"
  echo "read tokens: ", lex.tokens.high
  echo "EOF"
  echo "token stream: "
  echo "|", lex.tokens, "|"
  echo "new token stream: "
  echo "|", gen.tokens, "|"


main()