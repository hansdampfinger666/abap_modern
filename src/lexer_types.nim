import std/tables
import std/strutils


type
  TokenKind* {.pure.} = enum
    at_sign = "@"
    comma = ","
    dot = "."
    dquote = "\""
    equals = "="
    lbracket = "("
    lsbracket = "["
    linebreak = "\n"
    minus = "-"
    plus = "+"
    rbracket = ")"
    rsbracket = "]"
    squote = "\'"
    star = "*"
    wspace = " "
    add = "ADD"
    tand = "AND"
    corresponding_fields_of = "CORRESPONDING FIELDS OF"
    tdistinct = "DISTINCT"
    for_all_entries = "FOR ALL ENTRIES"
    tfrom = "FROM"
    into = "INTO"
    move = "MOVE"
    select = "SELECT"
    single = "SINGLE"
    table =  "TABLE"
    to = "TO"
    where = "WHERE"
    trailing_comment
    ident
    integer
    floating
    none
  TokenData* = ref object
    case kind*: TokenKind
    of ident, trailing_comment: strval*: string
    of floating:
      floatval*: float
      decimals*: int
    of integer: intval*: int
    else: discard
  TokenDatastore* = Table[int, TokenData]
  Token* = object  
    kind*: TokenKind
    data*: TokenData
  AliasRelation* = object
    alias*: char
    kind*: TokenKind
  Lexer* = object 
    line_number*: int
    current_start_tok_kind*: TokenKind
    tok_idx_first*: int
    tok_idx_last*: int
    read_chars*: int
    pos*: int
    read_pos*: int
    c*: char
    input*: string
    tokens*: string
    token_datastore*: TokenDatastore

const
  Keywords* = [
    TokenKind.add,
    TokenKind.tand,
    TokenKind.corresponding_fields_of,
    TokenKind.tdistinct,
    TokenKind.for_all_entries,
    TokenKind.tfrom,
    TokenKind.into,
    TokenKind.move,
    TokenKind.select,
    TokenKind.single,
    TokenKind.table,
    TokenKind.to,
    TokenKind.move,
    TokenKind.where,
  ]
  alias_mapping* = [
    AliasRelation(alias: '@', kind: TokenKind.at_sign),
    AliasRelation(alias: ',', kind: TokenKind.comma),
    AliasRelation(alias: '.', kind: TokenKind.dot),
    AliasRelation(alias: '\"', kind: TokenKind.dquote),
    AliasRelation(alias: '=', kind: TokenKind.equals),
    AliasRelation(alias: '(', kind: TokenKind.lbracket),
    AliasRelation(alias: '[', kind: TokenKind.lsbracket),
    AliasRelation(alias: '\n', kind: TokenKind.line_break),
    AliasRelation(alias: '-', kind: TokenKind.minus),
    AliasRelation(alias: '+', kind: TokenKind.plus),
    AliasRelation(alias: ')', kind: TokenKind.rbracket),
    AliasRelation(alias: ']', kind: TokenKind.rsbracket),
    AliasRelation(alias: '\'', kind: TokenKind.squote),
    AliasRelation(alias: '*', kind: TokenKind.star),
    AliasRelation(alias: ' ', kind: TokenKind.wspace),
    AliasRelation(alias: 'a', kind: TokenKind.add),
    AliasRelation(alias: 'A', kind: TokenKind.tand),
    AliasRelation(alias: 'C', kind: TokenKind.corresponding_fields_of),
    AliasRelation(alias: 'd', kind: TokenKind.tdistinct),
    AliasRelation(alias: 'e', kind: TokenKind.for_all_entries),
    AliasRelation(alias: 'F', kind: TokenKind.tfrom),
    AliasRelation(alias: 'I', kind: TokenKind.into),
    AliasRelation(alias: 'm', kind: TokenKind.move),
    AliasRelation(alias: 's', kind: TokenKind.select),
    AliasRelation(alias: 'S', kind: TokenKind.single),
    AliasRelation(alias: 'T', kind: TokenKind.table),
    AliasRelation(alias: 't', kind: TokenKind.to),
    AliasRelation(alias: 'w', kind: TokenKind.where),
    AliasRelation(alias: 'c', kind: TokenKind.trailing_comment),
    AliasRelation(alias: 'n', kind: TokenKind.ident),
    AliasRelation(alias: 'i', kind: TokenKind.integer),
    AliasRelation(alias: 'f', kind: TokenKind.floating),
    AliasRelation(alias: '\n', kind: TokenKind.linebreak),
  ]


proc translate*(alias: char): TokenKind =
  for relation in alias_mapping:
    if relation.alias == alias: return relation.kind
  assert false == true # should be unreachable

proc translate*(token_kind: TokenKind): char =
  for relation in alias_mapping:
    if relation.kind == token_kind: return relation.alias
  assert false == true # should be unreachable

proc `$`*(tok: Token): string =
  case tok.kind
  of trailing_comment, ident: 
    result = tok.data.strval
  of floating: 
    result = $(tok.data.floatval)
  of integer: result = $(tok.data.intval)
  of none: result = ""
  else: 
    for token_kind in TokenKind:
      if tok.kind == token_kind:
        result = $token_kind
  assert result != ""

proc token_strlen*(tok: Token): int =
  ($tok).len

proc create_string_from_tokenstream*(tokens: string, 
  token_datastore: TokenDatastore, first = 0, last = tokens.high): string =
  if first == -1 or last == -1: return
  for tok_idx, tok_alias in tokens[first..last]:
    let kind = tok_alias.translate()
    case kind
    of [trailing_comment, ident, integer, floating]:
      if not hasKey(token_datastore, tok_idx + first): 
        result &= "[error reading token data]"
      case token_datastore[tok_idx + first].kind
      of ident, trailing_comment: 
        result &= token_datastore[tok_idx + first].strval
      of floating: 
        result &= $token_datastore[tok_idx + first].floatval
      of integer: 
        result &= $token_datastore[tok_idx + first].intval
      else: discard
    else: result &= $kind

proc print_token_data*(token_datastore: TokenDatastore, first, last: int) =
  var data_str: string
  for tok_idx in first..last:
    if hasKey(token_datastore, tok_idx):
      case token_datastore[tok_idx].kind
      of ident, trailing_comment: 
        data_str &= "(" & $tok_idx & ")" & 
          token_datastore[tok_idx].strval & ", "
      of floating: 
        data_str &= "(" & $tok_idx & ")" & 
          $token_datastore[tok_idx].floatval & ", "
      of integer: 
        data_str &= "(" & $tok_idx & ")" & 
          $token_datastore[tok_idx].intval & ", "
      else: discard
  if data_str.len != 0:
    delete(data_str, data_str.high - 1..data_str.high)
    echo "data: ", data_str

proc print_token_stream*(lex: Lexer) =
  echo "token stream: |", lex.tokens, "|"