import std/strutils
import std/re
import std/tables

import lexer_types


const
  start_token_kinds = [
    TokenKind.add,
    TokenKind.move, 
    TokenKind.select,
  ]
  regex_mapping = {
    TokenKind.ident: "^[0-9A-Za-z_-]*",
    TokenKind.trailing_comment: "^[*" & "\"" & "].*",
    TokenKind.floating: r"^(\d+.\d+)",
    TokenKind.integer: r"^(\d+)"
  }.toTable()


proc initLexer*(): Lexer =
  result = Lexer()
  result.tok_idx_first = 0
  result.tok_idx_last = -1
  result.current_start_tok_kind = none

proc line_starts_with_start_token*(lex: var Lexer, line: string): bool =
  let stripped_line = strip(line)
  for kind in start_token_kinds:
    if stripped_line.startsWith($kind): 
      lex.current_start_tok_kind = kind
      return true
  return false

proc advance_tok_indexes*(lex: var Lexer) =
  if lex.tok_idx_first <= lex.tok_idx_last:
    lex.tok_idx_first = lex.tok_idx_last + 1

proc reset_lexer*(lex: var Lexer) =
  lex.pos = 0
  lex.read_pos = 0
  lex.c = '\0'

proc initToken*(kind: TokenKind, data: varargs[string]): Token =
  case kind:
  of ident, trailing_comment:
    result = Token(
      kind: kind,
      data: TokenData(
        kind: kind,
        strval: data[0]
      )
    )
  of floating: 
    var decimals = data[0].len - find(data[0], '.') - 1
    decimals = if decimals >= 0: decimals else: 0
    result = Token(
      kind: kind,
      data: TokenData(
        kind: kind,
        floatval: data[0].parseFloat(),
        decimals: decimals,
      )
    )
  of integer: 
    result = Token(
      kind: kind,
      data: TokenData(
        kind: kind,
        intval: data[0].parseInt()
      )
    )
  else: result = Token(kind: kind)

proc read_char*(lex: var Lexer): bool=
  if lex.read_pos >= lex.input.len:
    lex.c = '\0'
    return false
  lex.c = lex.input[lex.read_pos] 
  lex.pos = lex.read_pos
  lex.read_pos += 1
  lex.read_chars += 1
  return true

proc read_keyword(lex: Lexer): Token =
  result = Token(kind: none)
  for keyword in Keywords:
    if startsWith(lex.input[lex.pos..lex.input.high], $keyword):
      return initToken(keyword)

proc read_regex(lex: Lexer, kind: TokenKind): Token =
  let bounds = findBounds(
    lex.input[lex.pos..lex.input.high], re(regex_mapping[kind])
  )
  if bounds.first == -1: return Token(kind: none)
  return initToken(
    kind = kind,
    data = lex.input[bounds.first + lex.pos..bounds.last + lex.pos] 
  )

proc read_identifier(lex: Lexer): Token =
  result = read_regex(lex, ident)
  
proc read_trailing_comment(lex: Lexer): Token =
  read_regex(lex, trailing_comment)

proc read_float(lex: Lexer): Token =
  read_regex(lex, floating)

proc read_int(lex: var Lexer): Token =
  read_regex(lex, integer)

proc read_simple_token(lex: Lexer): Token =
  result = Token(kind: none)
  for token_kind in TokenKind:
    if $lex.c == $token_kind:
      return Token(kind: token_kind)

proc add_token*(lex: var Lexer, tok: Token) =
  inc lex.tok_idx_last
  lex.tokens.add(tok.kind.translate)
  lex.pos += token_strlen(tok)
  lex.read_pos = lex.pos
  if tok.data != nil:
    lex.token_datastore[lex.tokens.high] = tok.data

proc next_token*(lex : var Lexer) = 
  var tok: Token
  template get_token(read_proc: proc) = 
    tok = lex.read_proc()
    if tok.kind != TokenKind.none:
      add_token(lex, tok)
      return
  if lex.c.isAlphaAscii():
    get_token(read_keyword)
    get_token(read_identifier)
  elif lex.c in ['*', '"']:
    get_token(read_trailing_comment)
  elif lex.c.isDigit():
    get_token(read_float)
    get_token(read_int)
  else:
    get_token(read_simple_token)
  if tok.kind == none:
    lex.print_token_stream()
    echo "missing implementation to tokenize: ", 
      lex.input[lex.pos..lex.read_pos]
  assert tok.kind != TokenKind.none