# Package

version       = "0.1.0"
author        = "="
description   = "Modernize abap source files"
license       = "GPL-2.0-or-later"
srcDir        = "src"
bin           = @["abap_modern"]


# Dependencies

requires "nim >= 1.6.12"
