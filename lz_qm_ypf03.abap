*----------------------------------------------------------------------*
***INCLUDE LZ_QM_YPF03 .
*----------------------------------------------------------------------*

*&---------------------------------------------------------------------*
*&      Form  READ_INFO_RECORD
*&---------------------------------------------------------------------*
*   If there is no Info record data can not be recommended
*----------------------------------------------------------------------*
FORM read_info_record .

  CALL FUNCTION 'READ_LFB1'
    EXPORTING
      xbukrs         = ekko-bukrs
      xlifnr         = viqmel-lifnum
    IMPORTING
      xlfb1          = lfb1
    EXCEPTIONS
      key_incomplete = 1
      not_authorized = 2
      not_found      = 3
      OTHERS         = 4.

*  note 955621: no relevance between LFB1 and EINA
*  -> but EINA necessary for form READ_EINE
*  CHECK SY-SUBRC = 0.

  CALL FUNCTION 'ME_EINA_SINGL_READ_MATNR_LIFNR'
    EXPORTING
      pi_matnr         = viqmel-matnr
      pi_lifnr         = viqmel-lifnum
    IMPORTING
      po_eina          = eina
    EXCEPTIONS
      no_records_found = 1
      OTHERS           = 2.
  IF sy-subrc <> 0 OR NOT eina-loekz IS INITIAL.
    MESSAGE s191(qm).
    CLEAR eina.                                             "note 873067
  ENDIF.

ENDFORM.                    " READ_INFO_RECORD
*&---------------------------------------------------------------------*
*&      Form  READ_EINE
*&---------------------------------------------------------------------*
*      Nachlesen des Einkaufsinfosatzes
*----------------------------------------------------------------------*
FORM read_eine .
* note 873067
* -> read plant info record first
* -> check for deletion flags
* -> use no retail function module
  DATA: ls_incom      TYPE meico.

  IF eina IS INITIAL.     "no info record found in form READ_INFO_RECORD
    CLEAR eine.
    EXIT.
  ENDIF.

  CLEAR ls_incom.
  ls_incom-infnr = eina-infnr.
* ls_incom-ekorg = viqmel-ekorg.
  ls_incom-ekorg = ekko-ekorg.
  ls_incom-werks = viqmel-mawerk.
  ls_incom-esokz = '0'.  "read only normal info records
  ls_incom-onlye = 'X'.  "read only EINE
  ls_incom-leina = 'X'.  "read not using the buffer

* fm looks for plant info record first, returns the plant-independent
* if there is nothing for plant or there a deletion flag is active
  CALL FUNCTION 'ME_READ_INFORECORD'
    EXPORTING
      incom     = ls_incom
    IMPORTING
      einedaten = eine
    EXCEPTIONS
      OTHERS    = 1.

  IF sy-subrc <> 0 OR NOT eine-loekz IS INITIAL.
    CLEAR eine.
  ENDIF.
ENDFORM.                    " READ_EINE

*&---------------------------------------------------------------------*
*&      Form  BAPI_PO_CREATE1
*&---------------------------------------------------------------------*
FORM bapi_po_create1.

  CLEAR:
    g_exp_ebeln, gt_return.

*  Violation of note 1895028, therefore we continue to use the crapy
*  ekpo modify
*  DATA:
*    lt_bapiparex TYPE TABLE OF bapiparex,
*    ls_mepoitem  TYPE bapi_te_mepoitem.
*
*  INSERT INITIAL LINE INTO TABLE lt_bapiparex
*    ASSIGNING FIELD-SYMBOL(<fs_bapiparex>).
*
*  ls_mepoitem-po_item       = po_item[ lines( po_item ) ]-po_item.
*  ls_mepoitem-zzrobser      = viqmel-serialnr.
*  <fs_bapiparex>-structure  = 'BAPI_TE_MEPOITEM'.
*  <fs_bapiparex>-valuepart1 = ls_mepoitem+0(240).

** TODO, next time, use Z_QM_YP_PO_CREATE1 in dest 'NONE' instead of BAPI & Commit
  CALL FUNCTION 'BAPI_PO_CREATE1'
    EXPORTING
      poheader         = po_header
      poheaderx        = po_headerx
      no_messaging     = abap_false
      no_message_req   = abap_false
    IMPORTING
      exppurchaseorder = g_exp_ebeln
    TABLES
      return           = gt_return
      poitem           = po_item
      poitemx          = po_itemx
      poaccount        = po_account
      poaccountx       = po_accountx
*     extensionin      = lt_bapiparex
      poshippingexp    = po_expv.

  READ TABLE gt_return INTO gs_return WITH KEY type = 'E'.
  IF sy-subrc NE 0.
    CALL FUNCTION 'BAPI_TRANSACTION_COMMIT'.

    MESSAGE i018(z_qm_yp) WITH g_exp_ebeln.

    IF sy-subrc = 0.
      PERFORM add_sernr_to_item.
      PERFORM change_spart_in_ret_item.
    ENDIF.

  ELSE.
*    perform error_bapi_meldung .
    DATA: lt_help_value TYPE TABLE OF help_value,
          ls_help_value TYPE help_value,
          lt_string_out TYPE TABLE OF string,
          ls_string_out TYPE string,
          lt_return     TYPE bapiret2_tab,
          ls_return     TYPE bapiret2.

    REFRESH: lt_help_value, lt_string_out.
    ls_help_value-tabname = 'T100'.
    ls_help_value-fieldname = 'TEXT'.
    APPEND ls_help_value TO lt_help_value.

    LOOP AT gt_return INTO ls_return WHERE type = 'E'.
      APPEND ls_return TO lt_return.
      ls_string_out = ls_return-message.
      APPEND ls_string_out TO lt_string_out.
    ENDLOOP.
    CLEAR ls_return.
    MESSAGE w025(z_qm_yp) INTO ls_return-message.
    APPEND ls_return-message TO lt_string_out.

    CALL FUNCTION 'POPUP_TO_SHOW_DB_DATA_IN_TABLE'
      EXPORTING
        title_text        = 'Meldungen'(010)
      TABLES
        fields            = lt_help_value
        valuetab          = lt_string_out "gt_return
      EXCEPTIONS
        field_not_in_ddic = 1
        OTHERS            = 2.

    CALL FUNCTION 'BAPI_TRANSACTION_ROLLBACK'.
    RAISE action_stopped.

*    PERFORM error_bapi_meldung.
  ENDIF.
ENDFORM.                    " BAPI_PO_CREATE1

*&---------------------------------------------------------------------*
*&      Form  FILL_PO_HEADER
*&---------------------------------------------------------------------*
FORM fill_po_header.

  SELECT SINGLE waers FROM lfm1
    INTO @DATA(lv_curr)
    WHERE lifnr EQ @viqmel-lifnum
      AND ekorg EQ @ekko-ekorg.

  po_header = VALUE #(
    doc_type = ekko-bsart
    comp_code = ekko-bukrs
    purch_org = ekko-ekorg
    pur_group = ekko-ekgrp
    vendor = viqmel-lifnum
    created_by = sy-uname
    currency = lv_curr
  ).
  po_headerx = VALUE #(
    doc_type = abap_true
    comp_code = abap_true
    purch_org = abap_true
    pur_group = abap_true
    vendor = abap_true
    created_by = abap_true
    currency = abap_true
  ).

ENDFORM.                    " FILL_PO_HEADER

FORM fill_po_items USING pv_ebelp TYPE ebelp ps_viqmel TYPE viqmel pv_repo TYPE abap_bool.

  DATA:
    lv_bwkey TYPE mbew-bwkey,
    lv_verpr TYPE mbew-verpr,
    lv_peinh TYPE mbew-peinh,
    lv_ebelp TYPE ebelp.

  lv_ebelp = pv_ebelp.

  SELECT SINGLE bwkey FROM t001w
      INTO lv_bwkey
    WHERE werks = ps_viqmel-mawerk.
  IF sy-subrc = 0.
    SELECT SINGLE verpr peinh FROM mbew
        INTO (lv_verpr, lv_peinh)
      WHERE matnr = ps_viqmel-matnr
        AND bwkey = lv_bwkey
        AND lvorm = abap_false.
  ENDIF.

  " determine correct PO price, same problem as in QM Y4-notifs
  " (Verweinen)

  SELECT SINGLE waers FROM lfm1
    INTO @DATA(lv_curr)
    WHERE lifnr EQ @viqmel-lifnum
      AND ekorg EQ @ekko-ekorg.

  IF lv_curr IS INITIAL.
    lv_curr = 'EUR'.
  ELSEIF lv_curr NE 'EUR'.

    DATA lv_price TYPE i.

    CALL FUNCTION 'CONVERT_TO_LOCAL_CURRENCY'
      EXPORTING
        date             = sy-datum                 " Currency translation date
        foreign_amount   = lv_verpr                 " Amount in foreign currency
        foreign_currency = 'EUR'                    " Currency key for foreign currency
        local_currency   = lv_curr                  " Currency key for local currency
      IMPORTING
        local_amount     = lv_price                 " Amount in local currency
      EXCEPTIONS
        OTHERS           = 6.

    lv_verpr = lv_price.   " we need an implicit int to float conversion

  ENDIF.

**********************************************************************
* 1 Zeile
  CLEAR:
    po_item, po_itemx.

  MOVE 1.23         
    TO po_item-po_item.
  MOVE lv_ebelp         TO po_itemx-po_item.
  MOVE 'X'              TO po_itemx-po_itemx.
  MOVE ps_viqmel-matnr  TO po_item-material.
  MOVE 'X'              TO po_itemx-material.
  MOVE ps_viqmel-zzlgortein TO po_item-stge_loc.
  MOVE 'X'              TO po_itemx-stge_loc.
  MOVE ps_viqmel-mawerk TO po_item-plant.
  MOVE 'X'              TO po_itemx-plant.
  MOVE '2'              TO po_item-po_price. "Preisübernahme: 1 = Brutto, 2 = Netto
  MOVE 'X'              TO po_itemx-po_price.
* IT-Informatik, G.Ghotra                                     14.12.1016
* Preis aus Materialbewertung verwenden
  MOVE lv_verpr         TO po_item-net_price.
  MOVE 'X'              TO po_itemx-net_price.
  MOVE lv_peinh         TO po_item-price_unit.
  MOVE 'X'              TO po_itemx-price_unit.
*
  MOVE ps_viqmel-bzmng  TO po_item-quantity.
  MOVE 'X'              TO po_itemx-quantity.
  MOVE ps_viqmel-mgein  TO po_item-po_unit.
  MOVE  'X'             TO po_itemx-po_unit.
  MOVE ps_viqmel-qmnum  TO po_item-preq_name.
  MOVE 'X'              TO po_itemx-preq_name.

  IF pv_repo = abap_true.
    MOVE 'ZYP'      TO po_item-conf_ctrl.
    MOVE abap_true  TO po_itemx-conf_ctrl.
  ENDIF.

  APPEND po_item.
  APPEND po_itemx.

**********************************************************************
* 2. Zeile
  CLEAR:
    po_item, po_itemx.

  ADD 1.12 
    TO lv_ebelp.
  MOVE lv_ebelp         TO po_item-po_item.
  MOVE lv_ebelp         TO po_itemx-po_item.
  MOVE 'C'              TO po_item-acctasscat.
  MOVE 'X'              TO po_itemx-acctasscat.
  MOVE 'Rep.Kosten'     TO po_item-short_text.
  MOVE 'X'              TO po_itemx-short_text.
  MOVE '1'              TO po_item-quantity.
  MOVE 'X'              TO po_itemx-quantity.
  MOVE 'LE'             TO po_item-po_unit.
  MOVE 'X'              TO po_itemx-po_unit.
  MOVE '1'              TO po_item-net_price.   " has to be at least 1 (not 0.01) because some currencies do not support floats
  MOVE 'X'              TO po_itemx-net_price.
  MOVE ps_viqmel-qmnum  TO po_item-preq_name.
  MOVE 'X'              TO po_itemx-preq_name.
  MOVE ps_viqmel-mawerk TO po_item-plant.
  MOVE 'X'              TO po_itemx-plant.
  MOVE '43009000'       TO po_item-matl_group.
  MOVE 'X'              TO po_itemx-matl_group.

  IF pv_repo = abap_true.
    MOVE 'ZYP'       TO po_item-conf_ctrl.
    MOVE abap_true   TO po_itemx-conf_ctrl.
    MOVE abap_false  TO po_item-gr_ind.
    MOVE abap_true   TO po_itemx-gr_ind.
  ENDIF.

  APPEND po_item.
  APPEND po_itemx.

  CLEAR: po_account, po_accountx.

  MOVE lv_ebelp         TO po_account-po_item .
  MOVE lv_ebelp         TO po_accountx-po_item .
  MOVE cobl-saknr       TO po_account-gl_account.
  MOVE 'X'              TO po_accountx-gl_account.
  MOVE cobl-kdauf       TO po_account-sd_doc.
  MOVE 'X'              TO po_accountx-sd_doc.
  MOVE cobl-kdpos       TO po_account-itm_number .
  MOVE 'X'              TO po_accountx-itm_number .
* move cobl-kdein       to po_account-sched_line . "Einteilungsnummer
* move 'X'              to po_accountx-sched_line .
  MOVE vbap-gsber       TO po_account-bus_area .
  MOVE 'X'              TO po_accountx-bus_area .
  MOVE vbak-kokrs       TO po_account-co_area . "Kostenrechnungskreis
  MOVE 'X'              TO po_accountx-co_area .
  APPEND po_account.
  APPEND po_accountx.

**********************************************************************
* 3. Zeile
  CLEAR:
    po_item, po_itemx.

  ADD 1 TO lv_ebelp.
  MOVE lv_ebelp         TO po_item-po_item.
  MOVE lv_ebelp         TO po_itemx-po_item.
  MOVE ps_viqmel-matnr  TO po_item-material.
  MOVE 'X'              TO po_itemx-material.
  MOVE ps_viqmel-lgortvorg TO po_item-stge_loc.
  MOVE 'X'              TO po_itemx-stge_loc.
  MOVE ps_viqmel-mawerk TO po_item-plant.
  MOVE 'X'              TO po_itemx-plant.
  MOVE '2'              TO po_item-po_price. "Preisübernahme: 1 = Brutto, 2 = Netto
  MOVE 'X'              TO po_itemx-po_price.
* IT-Informatik, G.Ghotra                                     14.12.1016
* Preis aus Materialbewertung verwenden
  MOVE lv_verpr         TO po_item-net_price.
  MOVE 'X'              TO po_itemx-net_price.
  MOVE lv_peinh         TO po_item-price_unit.
  MOVE 'X'              TO po_itemx-price_unit.
*
  MOVE ps_viqmel-bzmng  TO po_item-quantity.
  MOVE 'X'              TO po_itemx-quantity.
  MOVE ps_viqmel-mgein  TO po_item-po_unit.
  MOVE 'X'              TO po_itemx-po_unit.
  MOVE 'X'              TO po_item-ret_item.
  MOVE 'X'              TO po_itemx-ret_item.
  MOVE ps_viqmel-qmnum  TO po_item-preq_name.
  MOVE 'X'              TO po_itemx-preq_name.

  IF pv_repo = abap_true.
    MOVE 'ZYP'            TO po_item-conf_ctrl.
    MOVE 'X'              TO po_itemx-conf_ctrl.
  ENDIF.

  APPEND po_item.
  APPEND po_itemx.

ENDFORM.

*&---------------------------------------------------------------------*
*&      Form  BAPI_PO_CHANGE
*&---------------------------------------------------------------------*
FORM bapi_po_change .

  CALL FUNCTION 'BAPI_PO_CHANGE'
    EXPORTING
      purchaseorder = g_exp_ebeln
    TABLES
      return        = gt_return
      poitem        = po_item
      poitemx       = po_itemx
      poaccount     = po_account
      poaccountx    = po_accountx.
  READ TABLE gt_return INTO gs_return WITH KEY type = 'E'.
  IF sy-subrc NE 0.
    CALL FUNCTION 'BAPI_TRANSACTION_COMMIT'.

    MESSAGE i019(z_qm_yp) WITH g_exp_ebeln.

    IF sy-subrc = 0.
      PERFORM add_sernr_to_item.
    ENDIF.

  ELSE.
*    perform error_bapi_meldung .
    DATA: lt_help_value TYPE TABLE OF help_value,
          ls_help_value TYPE help_value,
          lt_string_out TYPE TABLE OF string,
          ls_string_out TYPE string,
          ls_return     TYPE bapiret2.

    ls_help_value-tabname = 'T100'.
    ls_help_value-fieldname = 'TEXT'.
    APPEND ls_help_value TO lt_help_value.

    LOOP AT gt_return INTO ls_return WHERE type = 'E'.
      ls_string_out = ls_return-message.
      APPEND ls_string_out TO lt_string_out.
    ENDLOOP.

*    HELP_VALUE-
    CALL FUNCTION 'POPUP_TO_SHOW_DB_DATA_IN_TABLE'
      EXPORTING
        title_text        = 'Meldungen'
      TABLES
        fields            = lt_help_value
        valuetab          = lt_string_out
      EXCEPTIONS
        field_not_in_ddic = 1
        OTHERS            = 2.
    IF sy-subrc <> 0.
      PERFORM error_bapi_meldung.
    ENDIF.

    CALL FUNCTION 'BAPI_TRANSACTION_ROLLBACK'.
    CLEAR g_exp_ebeln.
  ENDIF.
ENDFORM.                    " BAPI_PO_CHANGE

*&---------------------------------------------------------------------*
*&      Form  ADD_SERNR_TO_ITEM
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM add_sernr_to_item .

  DATA: lv_step TYPE i.

  CLEAR: po_item.
  SORT po_item BY po_item DESCENDING.
  READ TABLE po_item INDEX 1.

  lv_step = 1.
  DO lv_step TIMES.
    CALL FUNCTION 'MM_ENQUEUE_DOCUMENT'
      EXPORTING
        i_ebeln         = g_exp_ebeln
        i_ebelp         = po_item-po_item
      EXCEPTIONS
        document_locked = 1
        system_failure  = 2
        parameter_error = 3
        OTHERS          = 4.
    IF sy-subrc = 0.
      CLEAR lv_step.
    ELSE.
      WAIT UP TO 3 SECONDS.
      lv_step = lv_step + 1.
      IF lv_step > 30.
        CLEAR lv_step.
      ENDIF.
    ENDIF.
  ENDDO.

  CALL FUNCTION 'MM_ENQUEUE_DOCUMENT'
    EXPORTING
      i_ebeln = g_exp_ebeln
      i_ebelp = po_item-po_item.

  CALL FUNCTION 'ME_EKPO_SINGLE_READ'
    EXPORTING
      pi_ebeln            = g_exp_ebeln
      pi_ebelp            = po_item-po_item
      pi_bypassing_buffer = 'X'
      pi_refresh_buffer   = 'X'
    IMPORTING
      po_ekpo             = ekpo
    EXCEPTIONS
      no_records_found    = 1
      OTHERS              = 2.
  IF sy-subrc = 0.
    ekpo-zzrobser = viqmel-serialnr.
    MODIFY ekpo.
  ENDIF.

  CALL FUNCTION 'MM_DEQUEUE_DOCUMENT'
    EXPORTING
      i_ebeln = g_exp_ebeln
      i_ebelp = po_item-po_item.

ENDFORM.                    " ADD_SERNR_TO_ITEM

*&---------------------------------------------------------------------*
*&      Form  change_spart_in_ret_item
*&---------------------------------------------------------------------*
FORM change_spart_in_ret_item.

  DATA:
    lv_step  TYPE i,
    ls_vd    TYPE zyp_versanddaten,
    ls_vst   TYPE vstel,
    ls_vetvg TYPE vetvg.

  CLEAR: po_item.

  READ TABLE po_item
    WITH KEY ret_item = 'X'.
  CHECK sy-subrc = 0.

  lv_step = 1.
  DO lv_step TIMES.
    CALL FUNCTION 'MM_ENQUEUE_DOCUMENT'
      EXPORTING
        i_ebeln         = g_exp_ebeln
        i_ebelp         = po_item-po_item
      EXCEPTIONS
        document_locked = 1
        system_failure  = 2
        parameter_error = 3
        OTHERS          = 4.
    IF sy-subrc = 0.
      CLEAR lv_step.
    ELSE.
      WAIT UP TO 3 SECONDS.
      lv_step = lv_step + 1.
      IF lv_step > 30.
        CLEAR lv_step.
      ENDIF.
    ENDIF.
  ENDDO.

  CALL FUNCTION 'MM_ENQUEUE_DOCUMENT'
    EXPORTING
      i_ebeln = g_exp_ebeln
      i_ebelp = po_item-po_item.

  CALL FUNCTION 'ME_EKPV_SINGLE_READ'
    EXPORTING
      pi_ebeln            = g_exp_ebeln
      pi_ebelp            = po_item-po_item
      pi_bypassing_buffer = 'X'
      pi_refresh_buffer   = 'X'
    IMPORTING
      po_ekpv             = ekpv
    EXCEPTIONS
      no_records_found    = 1
      OTHERS              = 2.
  IF sy-subrc = 0.
    SELECT SINGLE * INTO ls_vd
      FROM zyp_versanddaten
      WHERE vkorg = ekpv-vkorg.
    IF sy-subrc = 0.
      ekpv-spart = ls_vd-spart.
      ekpv-kzazu = ls_vd-kzazu.
      MODIFY ekpv.
    ENDIF.
  ENDIF.

  CALL FUNCTION 'MM_DEQUEUE_DOCUMENT'
    EXPORTING
      i_ebeln = g_exp_ebeln
      i_ebelp = po_item-po_item.

ENDFORM.                    "change_spart_in_ret_item

FORM update_first_po_item USING ps_viqmel TYPE viqmel CHANGING cv_ebeln TYPE ebeln.

  DATA:
    lv_bool TYPE abap_bool.

  FREE:
    po_item, po_itemx.

  IF cv_ebeln IS INITIAL.
    RETURN.
  ENDIF.

  FIELD-SYMBOLS:
    <fs_item>  LIKE LINE OF po_item,
    <fs_itemx> LIKE LINE OF po_itemx.

  SELECT SINGLE @abap_true FROM ekpo INTO @lv_bool
    WHERE ebeln = @i_ebeln
      AND ebelp = @gc_s_constants-po-ebelp
      AND matnr = @ps_viqmel-matnr.
  IF lv_bool = abap_false.
    INSERT INITIAL LINE INTO TABLE po_item
      ASSIGNING <fs_item>.
    INSERT INITIAL LINE INTO TABLE po_itemx
      ASSIGNING <fs_itemx>.
    <fs_item>-po_item   = gc_s_constants-po-ebelp.
    <fs_item>-material  = ps_viqmel-matnr.
    <fs_itemx>-po_item  = gc_s_constants-po-ebelp.
    <fs_itemx>-material = abap_true.
  ENDIF.

ENDFORM.

FORM check_po USING pt_ekko  TYPE tt_ekko
                    pv_qmnum TYPE qmnum
           CHANGING cv_ebeln TYPE ebeln
                    cv_ebelp TYPE ebelp
                    cv_bedat TYPE ebdat.

  TYPES:
    BEGIN OF ty_ekpo,
      ebeln TYPE ebeln,
      ebelp TYPE ebelp,
      bedat TYPE ebdat,
    END OF ty_ekpo.

  DATA:
    lt_ekpo TYPE TABLE OF ty_ekpo.

  FIELD-SYMBOLS:
    <fs_ekpo> TYPE ty_ekpo.

  CLEAR: cv_ebeln, cv_ebelp, cv_bedat.

  LOOP AT pt_ekko[] ASSIGNING FIELD-SYMBOL(<fs_ekko>).
    SELECT ekko~ebeln ebelp bedat INTO TABLE lt_ekpo
        FROM ekko
      JOIN ekpo
        ON ekpo~ebeln = ekko~ebeln
      WHERE ekko~ebeln = <fs_ekko>-ebeln
        AND afnam = pv_qmnum
        AND ekko~loekz <> 'L'
        AND ekpo~loekz <> 'L'.
    IF sy-subrc = 0.
      READ TABLE lt_ekpo
        WITH KEY ebelp = gc_s_constants-po-ebelp
        ASSIGNING <fs_ekpo>.
      IF sy-subrc <> 0.
        cv_ebeln = <fs_ekko>-ebeln.
        RETURN.
      ELSE.
        cv_ebeln = <fs_ekko>-ebeln.
        cv_ebelp = gc_s_constants-po-ebelp.
        cv_bedat = <fs_ekpo>-bedat.
        RETURN.
      ENDIF.
    ENDIF.
  ENDLOOP.

  FREE:
    lt_ekpo.

ENDFORM.

FORM update_customer_dynpro USING ps_viqmel TYPE viqmel.

  FIELD-SYMBOLS:
    <fs_viqmel> TYPE viqmel.

  ASSIGN ('(SAPLIQS0)VIQMEL') TO <fs_viqmel>.
  IF sy-subrc = 0.
    <fs_viqmel>-zzlgortein = ps_viqmel-zzlgortein.
  ENDIF.

ENDFORM.