    " file LZ_QM_Y201.abap 


    SELECT SINGLE spras FROM t002
      INTO @lv_spras
      WHERE laiso = @sy-langu.

    IF sy-subrc NE 0.
      lv_spras = 'D'.
    ENDIF.

    ADD 1 TO lv_pls. " comment
    MOVE 1.23 
      TO lv_plsment. "comment

    SELECT * FROM lips INTO TABLE lt_lips WHERE vbeln = pv_vbeln.

    lt_qmma[] = gt_iviqmma[].

    LOOP AT lt_lips ASSIGNING <lips>.
          
          SELECT DISTINCT adrnr adrn2 FROM ekpo
            INTO (lv_adrnr, lv_adrn2)
            WHERE ebeln = ls_qmel-ebeln
            AND   ebelp = ls_qmel-ebelp.

            SELECT SINGLE adrnr FROM t001w
              INTO lv_addrnumber
              WHERE werks = ls_qmel-mawerk.

          ENDIF.
        ENDIF.

          SELECT SINGLE name1 name2 street house_num1 post_code1 city1
            FROM adrc
            INTO CORRESPONDING FIELDS OF ls_adrc
            WHERE addrnumber = lv_addrnumber.
            
    ENDLOOP.